package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sample.Format.*;

public class Controller {
    public ImageView imageView;
    public ImageView imageView1;
    Format format;
    int x;
    int width;
    int height;
    int colorSize;
    int byteCount;
    WritableImage img;
    int xx;
    int yy;
    int red, green, blue;
    boolean painting;


    public void handleFilePicker(ActionEvent actionEvent) throws IOException, InterruptedException {
        initValues();
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(Stage.getWindows().stream().findFirst().get());

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsoluteFile())), 1000 * 8192);
        String thisLine;

        while ((thisLine = br.readLine()) != null && colorSize == 0) {
            while (!thisLine.isBlank()) {
                thisLine = tryToGetParam(thisLine);
            }
        }
        if (format == UNHANDLED) {
            System.out.println("Unhandled file format ");
            return;
        }
        getReadyForPainting();
        byteCount = colorSize > 255 ? 2 : 1;

        if (format == P3 && byteCount == 1) {
            do {
                while (!thisLine.isBlank()) {
                    thisLine = tryToSetColor(thisLine);
                }
            } while ((thisLine = br.readLine()) != null);
        } else if (format == P3 && byteCount == 2) {
            do {
                while (!thisLine.isBlank()) {
                    thisLine = tryToSetColorB2(thisLine);
                }
            } while ((thisLine = br.readLine()) != null);
        } else if (format == P6 && byteCount == 1) {
            do {
                tryToGetByte(thisLine);
            } while ((thisLine = br.readLine()) != null);
        } else if (format == P6 && byteCount == 2) {
            do {
                tryToGetByte2(thisLine);
            } while ((thisLine = br.readLine()) != null);
        }
    }

    private String tryToGetByte2(String thisLine) throws IOException {
        byte[] bytes = thisLine.getBytes();
        for (int i = 0; i < bytes.length; i = i + 2) {
            int val = 0;
            val |= bytes[i] & 0xFF;
            val <<= 8;
            val |= bytes[i + 1] & 0xFF;
            setColor(val / 256);
        }
        return "";
    }

    private String tryToGetByte(String thisLine) throws IOException {
        byte[] bytes = thisLine.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            setColor(bytes[i] & 0xFF);
        }
        return "";
    }

    public void setColor(int val) {
        if (x == 0) {
            red = val;
            x++;
        } else if (x == 1) {
            green = val;
            x++;
        } else if (x == 2) {
            blue = val;
            x++;
        }
        Color color = Color.rgb(red, green, blue);

        img.getPixelWriter().setColor((xx++) % width, (yy++) % height, color);

    }


    private void getReadyForPainting() {
        painting = true;
        x = 0;
        img = new WritableImage(width, height);
        imageView.setImage(img);
    }

    private void initValues() {
        format = UNHANDLED;
        x = 0;
        height = 0;
        width = 0;
        colorSize = 0;
        byteCount = 0;
        img = null;
        xx = 0;
        yy = 0;
        red = 0;
        green = 0;
        blue = 0;
        painting = false;
    }

    private String tryToGetParam(String thisLine) throws IOException {
        Pattern pat = Pattern.compile("\\s");
        int index = thisLine.indexOf('#');
        if (index != -1) {
            thisLine = thisLine.substring(0, index);
        }
        thisLine = thisLine.trim();
        while (!thisLine.isBlank()) {
            thisLine = thisLine.trim();
            Matcher m = pat.matcher(thisLine);
            if (m.find()) {
                index = m.start();
            } else index = -1;
            if (index != -1) {
                setParam(thisLine.substring(0, index));
                thisLine = thisLine.substring(index);
                thisLine = thisLine.trim();
            } else {
                setParam(thisLine);
                thisLine = "";
            }
        }
        return thisLine;

    }

    private String tryToSetColor(String thisLine) throws IOException {
        Pattern pat = Pattern.compile("\\s");
        int index = thisLine.indexOf('#');
        if (index != -1) {
            thisLine = thisLine.substring(0, index);
        }
        thisLine = thisLine.trim();
        while (!thisLine.isBlank()) {
            thisLine = thisLine.trim();
            Matcher m = pat.matcher(thisLine);
            if (m.find()) {
                index = m.start();
            } else index = -1;
            if (index != -1) {
                setColorP3(thisLine.substring(0, index));
                thisLine = thisLine.substring(index);
            } else {
                setColorP3(thisLine);
                thisLine = "";
            }
        }
        return thisLine;

    }

    private void setColorP3(String substring) {
        if (x == 0) {
            red = Integer.valueOf(substring);
            x++;

        } else if (x == 1) {
            green = Integer.valueOf(substring);
            x++;
        } else if (x == 2) {
            blue = Integer.valueOf(substring);
            x = 0;
            Color color = Color.rgb(red, green, blue);
            img.getPixelWriter().setColor((xx++) % width, (yy++) % height, color);
        }
    }


    private void setParam(String substring) {
        if (x == 0) {
            if (substring.equals("P6")) {
                format = P6;
                x++;
            }
            if (substring.equals("P3")) {
                format = P3;
                x++;
            }
        } else if (x == 1) {
            try {
                width = Integer.valueOf(substring);
                x++;

            } catch (NumberFormatException ex) {
                System.out.println("width");
            }

        } else if (x == 2) {
            try {
                height = Integer.valueOf(substring);
                x++;

            } catch (NumberFormatException ex) {
                System.out.println("height");
            }

        } else if (x == 3) {
            try {
                colorSize = Integer.valueOf(substring);
                x++;
            } catch (NumberFormatException ex) {
                System.out.println("size");
            }
        }
    }

    private String tryToSetColorB2(String thisLine) throws IOException {
        Pattern pat = Pattern.compile("\\s");
        int index = thisLine.indexOf('#');
        if (index != -1) {
            thisLine = thisLine.substring(0, index);
        }
        thisLine = thisLine.trim();
        while (!thisLine.isBlank()) {
            thisLine = thisLine.trim();
            Matcher m = pat.matcher(thisLine);
            if (m.find()) {
                index = m.start();
            } else index = -1;
            if (index != -1) {
                int val = Integer.valueOf(thisLine.substring(0, index));
                val = val / 256;
                setColorP3(String.valueOf(val));
                thisLine = thisLine.substring(index);
                thisLine = thisLine.trim();
            } else {
                int val = Integer.valueOf(thisLine);
                val = val / 256;
                setColorP3(String.valueOf(val));
                thisLine = "";
            }
        }
        return thisLine;

    }

    public void handleJPEGfilePicker(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(Stage.getWindows().stream().findFirst().get());

        BufferedImage image = ImageIO.read(file);
        imageView.setImage(SwingFXUtils.toFXImage(image, null));
        String filename = "file.jpg";
        float compressionParam=0.2f;

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("modal.fxml"));
        DialogPane myDialog = fxmlLoader.load();
        ModalController controller = fxmlLoader.getController();
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setDialogPane(myDialog);
        dialog.setTitle("Sasin 70M");
        Optional<ButtonType> clickedButton = dialog.showAndWait();
        if (clickedButton.isPresent()) {
            if (clickedButton.get() == ButtonType.OK) {
                filename = controller.input1.getCharacters().toString();
                try {
                    compressionParam = Float.valueOf(controller.input2.getCharacters().toString());
                }catch (NumberFormatException ex){
                    compressionParam = 0.2f;
                }
            }
        }


        File compressedImageFile = new File(filename);
        OutputStream os = new FileOutputStream(compressedImageFile);

        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
        ImageWriter writer = (ImageWriter) writers.next();

        ImageOutputStream ios = ImageIO.createImageOutputStream(os);
        writer.setOutput(ios);

        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        param.setCompressionQuality(compressionParam);  // Change the quality value you prefer
        writer.write(null, new IIOImage(image, null, null), param);

        imageView1.setImage(SwingFXUtils.toFXImage(ImageIO.read(compressedImageFile), null));
        os.close();
        ios.close();
        writer.dispose();
    }
}
